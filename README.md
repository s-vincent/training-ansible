# training-ansible

Ansible training files.

## Usages

This repository contains shellscripts and YAML files for practical exercices
regarding Ansible training course.

## License

All codes are under BSD-3 license.

