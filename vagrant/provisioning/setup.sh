#!/bin/bash

echo "Provisioning virtual machine..."

echo "Configure localtime"
if [ -z $TZ ]; then
  TZ="Europe/Paris"
fi

sudo rm /etc/localtime
sudo ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime

echo "Configure access to root account via public key"
sudo mkdir -p /root/.ssh
sudo cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

echo "Adds information to /etc/hosts"
cat <<EOF >> /etc/hosts
  10.67.0.10 vmcontroller
  10.67.0.11 vmdebian vmdebian.company.lan
  10.67.0.11 www.debian.lan
  10.67.0.12 vmalma vmalma.company.lan
  10.67.0.12 www.redhat.lan
EOF

if [ "$(hostname)" = "vmcontroller" ]; then
  yum install vim telnet tree unzip -y

  echo '& ~ # { } [ ] | ` \ ^ @ $ € < > !' > /home/vagrant/caracteres.txt

  cat <<EOF >> /home/vagrant/.vimrc
set ts=2
set st=2
set sw=2
set expandtab
set cindent
set tabstop=2
set softtabstop=2
set autoindent
set smartindent
set smarttab
set encoding=utf-8
set fileencoding=utf-8
syntax on
set sta
set et
set ai
set si
set cin
if has("autocmd")
filetype plugin indent on
endif
EOF
fi

if [ "$(hostname)" = "vmdebian" ]; then
  echo "Refresh apt repositories"
  apt-get update
elif [ "$(hostname)" = "vmalma" ]; then
  echo "Stop and disable firewalld"
  systemctl stop firewalld
  systemctl disable firewalld
fi


if [ "$(hostname)" = "vmdebian" -o "$(hostname)" = "vmalma" ]; then
  echo "Bring back sshd with password"
  sed -i 's/PasswordAuthentication.*/PasswordAuthentication yes/' /etc/ssh/sshd_config
  systemctl restart sshd
  echo "Set a password for vagrant user"
  echo "vagrant:vagrant" | chpasswd
fi

echo "End provisioning"

