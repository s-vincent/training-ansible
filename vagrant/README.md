# Vagrant setup

## Requirements

Install vagrant.

## VM

 * vmcontroller (10.67.0.10)
 * vmdebian (10.67.0.11)
 * vmalma (10.67.0.12)

## Provisioning

Provisioning script is present in `provisioning/setup.sh`.

## Start VM

```
vagrant up <name>
```

## Destroy VM

```
vagrant destroy <name> -f
```

## Connection

```
vagrant ssh <name>
```

